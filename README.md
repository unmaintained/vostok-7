About
-----
Vostok is a small set of customizations for the Android Open Source Project.
Unlike many other projects, it's just a set of patches on top of AOSP. Only
a few [own repositories](https://gitlab.com/groups/vostok) have been added.

Supported Devices
-----------------
| Name                         | Codename        | Branch            | Build  |
| ---------------------------- | --------------- | ----------------- | ------ |
| ASUS Nexus 7 2013 (Wi-Fi)    | flo             | android-7.1.2_r33 | NZH54D |
| HTC Nexus 9 (Wi-Fi)          | flounder        | android-7.1.2_r33 | NZH54D |
| LG Nexus 4                   | mako            | android-7.1.2_r33 | NZH54D |

Building
--------
Select the branch and codename of your device from the table above.

Download the source code:

    repo init -u https://android.googlesource.com/platform/manifest -b {BRANCH}
    mkdir -p .repo/local_manifests
    curl https://vostok.gitlab.io/vostok-7/vostok.xml > .repo/local_manifests/vostok.xml
    repo sync

You'll need a lot of disk space and some patience.

For **flounder** only: get proprietary binaries using
[this tool](https://github.com/anestisb/android-prepare-vendor):

    ./execute-all.sh -d flounder -a volantis -b {OBUILD} -o {DIR} --debugfs

where DIR is source code tree root; OBUILD is the latest
[official build](https://developers.google.com/android/images#volantis)
(it's different from BUILD in the table above).

Change current directory to the Android source tree root and build:

    source build/envsetup.sh    # add all needed commands into PATH
    lunch aosp_{CODENAME}-user  # select the target device
    ./vostok-7/apply            # apply customizations
    make dist -j `nproc`        # build OS (target files and fastboot image)

It will take some time to compile all the bits of the operating system. Output
images will be signed with test keys. Never flash those images! This is a huge
security risk because those keys are well-known.

You must generate your own set of release keys and keep them private (do this
only once):

    ./vostok-7/genkeys ../release-keys

Re-sign the images with your release keys:

    ./vostok-7/sign ../release-keys

You'll find the output in `out/dist/{BUILD}-{MMDD}.fastboot.zip`. Time to run
it!

Running
-------
Unlock the bootloader of your device, boot it into fastboot mode and flash the
images:

    fastboot update {BUILD}-{MMDD}.fastboot.zip

The device will reboot automatically. Welcome to your new system!
